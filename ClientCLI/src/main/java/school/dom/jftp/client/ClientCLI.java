package school.dom.jftp.client;

import school.dom.jftp.common.data.*;
import school.dom.jftp.common.io.LocalFileSystemAccessor;
import school.dom.jftp.common.util.JFTPConstants;

import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

public class ClientCLI {
    public static void main(String[] args) {
        fileAccessor.whitelistPath(Path.of(System.getProperty("user.dir")));

        final BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        final String argumentHelpText =
                "ClientCLI: Java File Transfer CLI Client\n" +
                        "usage: ClientCLI [options]\n\n" +
                        "Options:\n" +
                        "-h, --host:     Hostname to connect to.\n" +
                        "                Defaults to localhost.\n" +
                        "-p, --port:     Port number of the server to connect to." +
                        "                Defaults to " + JFTPConstants.DEFAULT_SERVER_PORT + ".\n" +
                        "-u, --username: Username to use when connecting.\n" +
                        "                Defaults to anon.\n\n" +
                        "--help:         Shows this help text.";

        final String commandHelpText =
                "Use HELP [command] for more information on each individual command.\n" +
                        "LS LL DIR\nGET\nPUT\nCD\nSTATUS\nQUIT EXIT\n? HELP";

        String hostname = "localhost";
        int port = JFTPConstants.DEFAULT_SERVER_PORT;
        String username = "anon";

        // Argument Parsing
        for (int i = 0; i < args.length; i++) {
            final String arg = args[i];
            switch (arg) {
                case "-h", "--host" -> {
                    hostname = args[i + 1];
                    i++;
                }
                case "-p", "--port" -> {
                    port = Integer.parseInt(args[i + 1]);
                    i++;
                }
                case "-u", "--username" -> {
                    username = args[i + 1];
                    i++;
                }
                case "--help" -> {
                    System.out.println(argumentHelpText);
                    return;
                }
            }
        }

        char[] password = passwordPrompt();
        if (password.length == 0) {
            System.err.println("WARNING: password is blank. Console might not have been instantiated.");
        }

        // Instantiate connection objects
        final Socket server;
        final FileClient client;
        try {
            server = new Socket(hostname, port);
            client = new FileClient(server);
        } catch (IOException e) {
            System.err.println("ERROR: " + e);
            return;
        }

        // Initialize connection
        String startPath;
        try {
            AuthorizationData auth = new AuthorizationData(username, new String(password));
            Arrays.fill(password, ' ');
            AuthorizationResponse response = client.authenticate(auth);
            if (response.resultCode().equals(AuthorizationResponse.Result.OK)) {
                startPath = response.startPath();
            } else {
                System.err.println("ERROR: invalid credentials, server denied connection.");
                client.closeConnection();
                return;
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("ERROR: " + e);
            client.closeConnection();
            return;
        }

        // Main loop
        try {
            List<TransferProgressInfo> transferHistory = new ArrayList<>();
            String displayPath = startPath;
            boolean shouldClose = false;
            while (!shouldClose) {
                transferHistory.addAll(
                        collectCompletedTransfers(client.fetchTransferProgress())
                );

                client.clearCancelledDownloads();
                client.clearCompletedTransfers();

                char prompt = (transferHistory.isEmpty()) ? '$' : '#';
                System.out.printf("[%s@%s %s]%c ", username, hostname, displayPath, prompt);
                String[] input = keyboard.readLine().split(" ", 2);
                String cmd = input[0];

                switch (cmd.toUpperCase()) {
                    case "?", "HELP" -> {
                        if (input.length == 1) {
                            System.out.println(commandHelpText);
                        } else {
                            String cmdHelp = input[1];
                            switch (cmdHelp.toUpperCase()) {
                                case "LS", "DIR" -> System.out.println(
                                        cmdHelp + ": List contents of a remote directory.\n" +
                                                "usage: " + cmdHelp + " [remote path]\n" +
                                                "[remote path]: The remote directory whose listing is being requested.\n" +
                                                "               Defaults to the current remote directory."
                                );
                                case "GET" -> System.out.println(
                                        cmdHelp + ": Enqueue the download of a resource on the server.\n" +
                                                "usage: " + cmdHelp + " filepath\n" +
                                                "filepath: The path of the remote file to be downloaded."
                                );
                                case "PUT" -> System.out.println(
                                        cmdHelp + ": Enqueue the upload of a resource to the server.\n" +
                                                "usage: " + cmdHelp + " localfilepath\n" +
                                                "localfilepath: The path of the remote file to be downloaded."
                                );
                                case "CD" -> System.out.println(
                                        cmdHelp + ": Change current working directory on the remote server.\n" +
                                                "usage: " + cmdHelp + " [remote path]\n" +
                                                "[remote path]: The remote directory to enter.\n" +
                                                "               Defaults to the server's starting directory for this user."
                                );
                                case "STATUS" -> System.out.println(
                                        cmdHelp + ": Display current download status. Will be invoked if no command is given.\n" +
                                                "usage: " + cmdHelp
                                );
                                case "?", "HELP" -> System.out.println(commandHelpText);
                                case "QUIT", "EXIT" -> System.out.println(
                                        cmdHelp + ": Disconnects from server and closes the application.\n" +
                                                "usage: " + cmdHelp
                                );
                                default -> System.err.println("ERROR: Command " + cmdHelp + " does not exist.\n");
                            }
                        }
                    }
                    case "LS", "LL", "DIR" -> {
                        String requestedPath = client.getCurrentRemotePath();
                        if (input.length > 1) {
                            if (requestedPath.isEmpty())
                                requestedPath = input[1]; // no leading /
                            else
                                requestedPath = requestedPath + '/' + input[1]; // / needed
                        }
                        ListingResponse r = client.getDirectoryListing(new ListingRequest(requestedPath));
                        if (!r.resultCode().equals(ListingResponse.Result.OK)) {
                            System.err.println("ERROR: Could not access requested directory.");
                        } else {
                            if (cmd.equalsIgnoreCase("LL"))
                                printLongListing(r.dirListing());
                            else
                                printListing(r.dirListing());
                        }
                    }
                    case "GET" -> {
                        if (input.length > 1) {
                            String remotePath = client.getCurrentRemotePath();
                            if (remotePath.isEmpty())
                                remotePath = input[1]; // no leading /
                            else
                                remotePath = remotePath + '/' + input[1]; // / needed
                            if (client.getFile(remotePath).isEmpty()) {
                                System.err.println("ERROR: Could not initiate file download.");
                            }
                        }
                    }
                    case "PUT" -> {
                        if (input.length > 1) {
                            String remotePath = client.getCurrentRemotePath();
                            String localPath = Path.of(rootDir, input[1]).toString();
                            if (remotePath.isEmpty())
                                remotePath = input[1]; // no leading /
                            else
                                remotePath = remotePath + '/' + input[1]; // / needed
                            if (client.putFile(localPath, remotePath).isEmpty())
                                System.err.println("ERROR: Could not initiate file download.");
                        }
                    }
                    case "CD" -> {
                        final String currentPath = client.getCurrentRemotePath();
                        String requestedPath = "";
                        if (input.length > 1) {
                            if (currentPath.isEmpty())
                                requestedPath = input[1];
                            else
                                requestedPath = currentPath + '/' + input[1];
                        }
                        ListingResponse r = client.changeDirectory(new ListingRequest(requestedPath));
                        if (!r.resultCode().equals(ListingResponse.Result.OK))
                            System.err.println("ERROR: Could not access specified directory.");
                        else
                            displayPath = r.dirPath();
                    }
                    case "", "STATUS" -> {
                        printTransferStatus(transferHistory.stream());
                        printTransferStatus(client.fetchTransferProgress());
                        client.clearCancelledDownloads();
                        client.clearCompletedTransfers();
                        transferHistory.clear();
                    }
                    case "QUIT", "EXIT" -> {
                        shouldClose = true;
                    }
                    default -> {
                        System.err.println("ERROR: Command " + cmd + " not recognized.");
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("ERROR: " + e);
        } finally {
            client.closeConnection();
        }
    }

    public static char[] passwordPrompt() {
        final Console console = System.console();
        if (console == null) {
            return new char[0];
        }

        return console.readPassword("Password: ");
    }

    public static void printListing(Collection<FileMetadata> dirListing) {
        dirListing.stream()
                .sorted(FileMetadata::compareToByExtension)
                .forEach(file -> System.out.println(file.name()));
    }

    public static void printTransferStatus(Stream<TransferProgressInfo> transfers) {
        transfers
                .forEach(tr -> System.out.println(tr.transferInfoAsString()));
    }

    public static void printLongListing(Collection<FileMetadata> dirListing) {
        dirListing.stream()
                .sorted(FileMetadata::compareToByExtension)
                .forEach(file -> {
                    if (file.isDir()) {
                        System.out.printf("drwx -------- %s %s\n",
                                JFTPConstants.TIMESTAMP_FORMATTER.format(file.timestampDate()),
                                file.name()
                        );
                    } else {
                        System.out.printf("-rwx %5.1f %2s %s %s\n",
                                file.scaledSize(file.getSizeScale()),
                                file.getSizeScale(),
                                JFTPConstants.TIMESTAMP_FORMATTER.format(file.timestampDate()),
                                file.name()
                        );
                    }
                });
    }

    public static List<TransferProgressInfo> collectCompletedTransfers(Stream<TransferProgressInfo> transfers) {
        return transfers
                .filter(TransferProgressInfo::isComplete)
                .toList();
    }

    public static final String rootDir = System.getProperty("user.dir");
    public static final LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
}
