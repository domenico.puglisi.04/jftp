package school.dom.jftp.server;

import school.dom.jftp.common.io.LocalFileSystemAccessor;
import school.dom.jftp.common.util.JFTPConstants;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    public static void main(String[] args) {
        PARENT_LOGGER.setLevel(Level.ALL);

        FileHandler fileErr;
        try {
            fileErr = new FileHandler("%t/JFTPServer%g.log");
            fileErr.setLevel(Level.ALL);
        } catch (IOException e) {
            System.err.println("COULD NOT CREATE LOG FILE. TERMINATING PROCESS.");
            return;
        }

        if(!fileAccessor.whitelistPath(rootDir)) {
            PARENT_LOGGER.warning("Could not register " + rootDir + " to whitelisted paths.");
            return;
        }

        ServerSocket ss;
        try {
            ss = new ServerSocket(JFTPConstants.DEFAULT_SERVER_PORT);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


        final Properties configData = new Properties();
        final String configFileName = "JFTPServer.xml";
        final Path configFilePath = Path.of(configFileName);

        if (!Files.exists(configFilePath)) {
            configData.setProperty("user.anon", JFTPConstants.CONFIG_NO_PASSWORD);
            try (FileOutputStream xmlConfigOut = new FileOutputStream(configFileName)) {
                configData.storeToXML(xmlConfigOut, "Default config data", StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        try (FileInputStream xmlConfigIn = new FileInputStream(configFileName)) {
            configData.loadFromXML(xmlConfigIn);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Map<String, String> userAuthData = new HashMap<>();
        for (String key : configData.stringPropertyNames()) {
            String val = configData.getProperty(key);

            if (key.startsWith("user.") && !key.equals("user.")) {
                String username = key.substring(key.indexOf('.') + 1);
                userAuthData.put(username, val);
            } else if (key.equals("dirs")) {
                String[] dirs = val.split(";");
                for (String dir: dirs) {
                    Path dirPath = Path.of(dir);
                    if (!fileAccessor.whitelistPath(dirPath))
                        PARENT_LOGGER.warning("Path could not be whitelisted: " + dirPath);
                }
            } else {
                PARENT_LOGGER.warning("Unrecognized key in config: " + key);
            }
        }

        ExecutorService threadPool = Executors.newFixedThreadPool(JFTPConstants.MAX_NUM_THREADS);
        while(true) {
            Socket client;
            try {
                client = ss.accept();
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            FileServer fileServer;
            try {
                fileServer = new FileServer(client, userAuthData);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            threadPool.execute(fileServer);
        }
    }

    private static final Logger PARENT_LOGGER = Logger.getLogger("");
    private static final LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
    private static final Path rootDir = Path.of(System.getProperty("user.dir"));
}
