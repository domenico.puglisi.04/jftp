package school.dom.jftp.server;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.io.FileAccessMode;
import school.dom.jftp.common.io.TransferHandler;
import school.dom.jftp.common.util.BlockingObjectOutputStream;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

public class DebugUploadHandler implements TransferHandler {
    public DebugUploadHandler(BlockingObjectOutputStream uploadStream, Path inputFilePath, int blockSize) {
        this.uploadStream = uploadStream;
        this.inputFilePath = inputFilePath;
        // this.blockSize = blockSize;
    }

    @Override
    public boolean onTransferRegistered(int uploadId) {
        System.out.printf("-- REGISTERING UPLOAD --\nID: %d\n\n", uploadId);
        this.uploadId = uploadId;
        return true;
    }

    @Override
    public void onTransferFinished() {
        System.out.printf("-- FINISHING UPLOAD --\nID: %d\n\n", uploadId);
    }

    @Override
    public void onTransferCancelled() {
        System.out.printf("-- ABORTING UPLOAD --\nID: %d\n\n", uploadId);
    }

    @Override
    public void onBlockProcessed(FileBlock block) throws IOException {
        System.out.printf("-- BLOCK SENT --\nID: %d\nSEQUENCE NUMBER: %d\n",
                block.operationId(), block.blockSeqNum());
        System.out.printf("%s\n", Arrays.toString(block.buffer()));

        try {
            uploadStream.writeObject(block);
            uploadStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public Path getTransferFilePath() {
        return inputFilePath;
    }

    @Override
    public FileAccessMode getTransferFileMode() {
        return FileAccessMode.READ;
    }

    private final Path inputFilePath;
    // private final int blockSize;
    private final BlockingObjectOutputStream uploadStream;

    private int uploadId;
}
