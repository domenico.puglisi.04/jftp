package school.dom.jftp.server;

import school.dom.jftp.common.data.*;
import school.dom.jftp.common.io.*;
import school.dom.jftp.common.util.BlockingObjectOutputStream;
import school.dom.jftp.common.util.JFTPConstants;

import java.io.*;
import java.net.Socket;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class FileServer implements Runnable {
    public FileServer(Socket client, Map<String, String> userAuthData)
            throws IOException {
        this.client = client;
        this.userAuthData = userAuthData;
        serializeOut = new BlockingObjectOutputStream(client.getOutputStream());
        // Flush the output so that the corresponding input may be constructed
        serializeOut.flush();
        serializeIn = new ObjectInputStream(client.getInputStream());
    }

    @Override
    public void run() {
        try {
            AuthorizationData auth = (AuthorizationData) serializeIn.readObject();
            if (!handleAuth(auth)) {
                LOGGER.info("Denied connection from " + client.getInetAddress());
                return;
            }
        } catch (EOFException e) {
            return;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        LOGGER.info("Accepted connection from " + client.getInetAddress());
        try {
            while (true) {
                Object incomingData;
                try {
                    incomingData = serializeIn.readObject();
                } catch (EOFException e) {
                    return;
                } catch (ClassNotFoundException | IOException e) {
                    e.printStackTrace();
                    return;
                }

                try {
                    if (incomingData instanceof GETRequest get) {
                        handleGet(get);
                    } else if (incomingData instanceof PUTRequest put) {
                        handlePut(put);
                    } else if (incomingData instanceof ListingRequest dirListingRequest) {
                        handleLs(dirListingRequest);
                    } else if (incomingData instanceof FileBlock block) {
                        handleBlock(block);
                    } else {
                        LOGGER.warning("Could not handle " + incomingData + " object.");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        } finally {
            LOGGER.info("Closing connection with " + client.getInetAddress());
            closeConnection();
        }
    }

    public boolean handleAuth(AuthorizationData auth) throws IOException {
        String storedPassword = userAuthData.get(auth.username());

        final AuthorizationResponse authResponse;
        if (!storedPassword.equals(JFTPConstants.CONFIG_NO_PASSWORD) && !auth.password().equals(storedPassword)) {
            authResponse = new AuthorizationResponse(AuthorizationResponse.Result.DENY);
            serializeOut.writeObject(authResponse);
            serializeOut.flush();
            return false;
        }

        var dirListing = getDirListing(rootDir);
        if(dirListing.isPresent()) {
            authResponse = new AuthorizationResponse(AuthorizationResponse.Result.OK, dirListing.get(),
                    String.valueOf(rootDir));
        } else {
            authResponse = new AuthorizationResponse(AuthorizationResponse.Result.DENY);
        }

        serializeOut.writeObject(authResponse);
        serializeOut.flush();
        return dirListing.isPresent();
    }

    public boolean handleGet(GETRequest get) throws IOException {
        final Path requestedFilePath = Path.of(get.filePath()).toAbsolutePath();

        final TransferHandler handler = new FileUploadHandler(serializeOut, requestedFilePath);
        final int operationId = handler.hashCode();

        final BlockSource fileReader;
        final FileMetadata fileInfo;
        try {
            fileReader = new FileBlockReader(requestedFilePath, operationId, get.blockSize());
            fileInfo = FileMetadata.fromPath(requestedFilePath);
        } catch (IOException e) {
            e.printStackTrace();
            serializeOut.writeObject(new GETResponse(GETResponse.Result.DENY));
            serializeOut.flush();
            return false;
        }

        TransferManager task = new TransferManager(operationId, handler, fileReader);
        if (!task.acquireResources()) {
            serializeOut.writeObject(new GETResponse(GETResponse.Result.DENY));
            serializeOut.flush();
            return false;
        }

        final GETResponse response = new GETResponse(GETResponse.Result.OK, fileInfo, operationId, get.blockSize());
        serializeOut.writeObject(response);
        serializeOut.flush();
        transferPool.execute(task);
        return true;
    }

    public boolean handlePut(PUTRequest put) throws IOException {
        final Path requestedFilePath = Path.of(put.newFilePath()).toAbsolutePath();
        final int lastBlock = (int) (put.fileSize() / put.blockSize());

        final TransferHandler handler = new FileDownloadHandler(requestedFilePath);
        final int operationId = handler.hashCode();

        final BlockingQueue<FileBlock> outputQueue = new LinkedBlockingQueue<>();
        final QueueReader blockReader = new QueueReader(outputQueue, lastBlock);
        downloadBlockMap.put(operationId, outputQueue);

        final TransferManager task = new TransferManager(
                operationId, handler, blockReader
        );

        if (!task.acquireResources()) {
            serializeOut.writeObject(new PUTResponse(PUTResponse.Result.DENY));
            serializeOut.flush();
            return false;
        }

        try {
            final PUTResponse response = new PUTResponse(PUTResponse.Result.OK, operationId);
            serializeOut.writeObject(response);
        } catch (IOException e) {
            e.printStackTrace();
            serializeOut.writeObject(new PUTResponse(PUTResponse.Result.DENY));
            serializeOut.flush();
            return false;
        }

        transferPool.execute(task);
        return true;
    }

    public boolean handleLs(ListingRequest ls) throws IOException {
        ListingResponse lsResponse;

        final Path requestedPath;
        try {
            requestedPath = Path.of(ls.dirPath()).toAbsolutePath().toRealPath();
        } catch (IOException | InvalidPathException e) {
            lsResponse = new ListingResponse(ListingResponse.Result.DENY);
            serializeOut.writeObject(lsResponse);
            serializeOut.flush();
            return false;
        }

        if (!fileAccessor.isPathInWhitelist(requestedPath)) {
            lsResponse = new ListingResponse(ListingResponse.Result.DENY);
            serializeOut.writeObject(lsResponse);
            serializeOut.flush();
            return false;
        }

        final var dirListing = getDirListing(requestedPath);
        if (dirListing.isPresent()) {
            lsResponse = new ListingResponse(ListingResponse.Result.OK, dirListing.get(), String.valueOf(requestedPath));
        } else {
            lsResponse = new ListingResponse(ListingResponse.Result.DENY);
        }

        serializeOut.writeObject(lsResponse);
        serializeOut.flush();
        return dirListing.isPresent();
    }

    public void handleBlock(FileBlock block) {
        var queue = downloadBlockMap.get(block.operationId());
        if (queue == null) {
            LOGGER.warning("Received block for invalid transfer: " + block);
            return;
        }

        if (!queue.add(block)) {
            LOGGER.warning("Could not enqueue the following valid block: " + block);
        }
    }

    public Optional<HashSet<FileMetadata>> getDirListing(Path dirPath) {
        if(!Files.isDirectory(dirPath))
            return Optional.empty();

        HashSet<FileMetadata> dirListing = new HashSet<>();

        try (Stream<Path> paths = Files.list(dirPath)) {
            paths.forEach(path -> {
                if (fileAccessor.isPathAvailableForReading(path)) {
                    try {
                        dirListing.add(FileMetadata.fromPath(path));
                    } catch (IOException e) {
                        LOGGER.warning("Exception occurred while trying to fetch file metadata;");
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            LOGGER.warning("Exception occurred while trying to fetch dir contents;");
            e.printStackTrace();
        }

        return Optional.of(dirListing);
    }

    public void closeConnection() {
        transferPool.shutdown();

        try {
            if (!transferPool.awaitTermination(10, TimeUnit.SECONDS)) {
                transferPool.shutdownNow();
            }
        } catch (InterruptedException e) {
            transferPool.shutdownNow();
        }

        try {
            serializeOut.flush();
            serializeOut.close();
            client.close();
        } catch (IOException ignored) {
            // Exceptions are ignored since the connection will be closed regardless
        } finally {
            Thread.currentThread().interrupt();
        }
    }

    private final BlockingObjectOutputStream serializeOut;
    private final ObjectInputStream serializeIn;
    private final Socket client;

    private final ConcurrentMap<Integer, BlockingQueue<FileBlock>> downloadBlockMap = new ConcurrentHashMap<>();
    private final ExecutorService transferPool = Executors.newFixedThreadPool(JFTPConstants.MAX_NUM_THREADS);

    private final Map<String, String> userAuthData;
    private static final Logger LOGGER = Logger.getLogger(FileServer.class.getName());
    private static final LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
    private static final Path rootDir = Path.of(System.getProperty("user.dir"));
}
