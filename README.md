# JFTP

Simple Client/Server file transfer application written in Java.

## Descrizione del Protocollo di Comunicazione Utilizzato

Per realizzare l'applicazione di rete, innanzitutto, sono stati suddivisi i componenti necessari per la realizzazione di client e server in cinque moduli separati:

- Common, per tutte le strutture dati e classi comuni a entrambe le applicazioni;

- ClientBase e ServerBase, che implementano l'effettivo protocollo di rete e forniscono le classi necessarie per offrire un'interfaccia utente finale;

- ClientCLI e ClientGUI, rispettivamente un client a riga di comando e uno con interfaccia grafica realizzata in JavaFX.

Il protocollo di comunicazione tra client e server si basa sull'uso di una serie di strutture dati comuni per trasmettere informazioni su Richieste e Risposte "JFTP", serializzate mediante l'uso di stream di serializzazione della libreria standard di Java.


Il client inizializza la comunicazione inviando al server una richiesta di autorizzazione contenente le credenziali dell'utente della sessione. Il server trasmette quindi una risposta che, nel caso di esito positivo, conterrà anche una lista dei file contenuti nella directory "radice" configurata.



Dopo questa fase iniziale, il client può trasmettere una serie di richieste diverse al server, in base all'input dell'utente. Sono supportate richieste di download e upload dei file (GET e PUT), e di listing dei contenuti delle directory remote (LS o DIR).
La trasmissione dei file avviene mediante la classe TransferManager, con l'ausilio di due ulteriori oggetti che implementano le interfacce BlockSource (per leggere blocchi di file da disco o socket) e TransferHandler (per scrivere blocchi di file su disco o mediante socket).

Suddividendo i file in blocchi, è possibile inoltre effettuare download e upload paralleli di file, monitorando il progresso dell'operazione.
Tramite la classe singleton LocalFileSystemAccessor, è possibile regolare l'accesso in lettura o scrittura a file o directory locali, evitando potenziali collisioni.

Infine, il server può essere configurato per offrire ai client l'accesso a ulteriori directory, e con utenti dalle credenziali differenti. Non è prevista la possibilità di limitare i permessi di lettura/scrittura in base all'utente, tuttavia.
