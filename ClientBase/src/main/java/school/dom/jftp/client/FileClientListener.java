package school.dom.jftp.client;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.io.QueueReader;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

public class FileClientListener implements Runnable {
    public FileClientListener(ObjectInputStream serverInput) {
        this.serverInput = serverInput;
    }

    @Override
    public void run() {
        while(true) {
            Object r;
            try {
                r = serverInput.readObject();
            } catch (IOException | ClassNotFoundException e) {
                // Log errors to file?
                return;
            }

            if (r instanceof FileBlock block) {
                incomingBlocksPerDownload.putIfAbsent(block.operationId(), new LinkedBlockingQueue<>());
                incomingBlocksPerDownload.get(block.operationId()).add(block);
            } else {
                serverResponses.add(r);
            }
        }
    }

    public Object receive() {
        Object retval = null;
        try {
            retval = serverResponses.take();
        } catch (InterruptedException ignored) {}

        return retval;
    }

    public QueueReader incomingBlocksAsQueue(Integer downloadId, int lastBlockNum) {
        incomingBlocksPerDownload.putIfAbsent(downloadId, new LinkedBlockingQueue<>());
        return new QueueReader(incomingBlocksPerDownload.get(downloadId), lastBlockNum);
    }

    private final BlockingQueue<Object> serverResponses = new LinkedBlockingQueue<>();
    private final ConcurrentMap<Integer, BlockingQueue<FileBlock>> incomingBlocksPerDownload = new ConcurrentHashMap<>();
    private final ObjectInputStream serverInput;
}
