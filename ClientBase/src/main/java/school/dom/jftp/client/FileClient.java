package school.dom.jftp.client;

import school.dom.jftp.common.data.*;
import school.dom.jftp.common.io.*;
import school.dom.jftp.common.util.BlockingObjectOutputStream;
import school.dom.jftp.common.util.JFTPConstants;

import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Stream;

public class FileClient {
    public FileClient(Socket server) throws IOException {
        this.server = server;
        serializeOut = new BlockingObjectOutputStream(server.getOutputStream());
        // Flush the output so that the corresponding input may be constructed
        serializeOut.flush();
        listener = new FileClientListener(new ObjectInputStream(server.getInputStream()));
        listenerThread = new Thread(listener);
        listenerThread.start();
    }

    public Object receive() {
        return listener.receive();
    }

    public AuthorizationResponse authenticate(AuthorizationData authInfo)
            throws IOException, ClassNotFoundException {
        serializeOut.writeObject(authInfo);
        serializeOut.flush();
        return (AuthorizationResponse) listener.receive();
    }

    public ListingResponse getDirectoryListing(ListingRequest remoteDir)
            throws IOException, ClassNotFoundException {
        serializeOut.writeObject(remoteDir);
        serializeOut.flush();
        return (ListingResponse) listener.receive();
    }

    public ListingResponse changeDirectory(ListingRequest remoteDir)
            throws IOException, ClassNotFoundException {
        var newDirListing = getDirectoryListing(remoteDir);
        if(newDirListing.resultCode().equals(ListingResponse.Result.OK)) {
            setRemotePath(remoteDir.dirPath());
        }

        return newDirListing;
    }

    public Optional<TransferProgressInfo> getFile(String remotePath)
            throws IOException, ClassNotFoundException {
        final GETRequest req = new GETRequest(remotePath);
        serializeOut.writeObject(req);

        final GETResponse getResponse = (GETResponse) listener.receive();
        if(!getResponse.resultCode().equals(GETResponse.Result.OK)) {
            return Optional.empty();
        }

        // TODO: Check if filesystem can hold new file
        final FileMetadata fileInfo = getResponse.fileInfo();
        final Path filePath = Path.of(fileInfo.name());
        final int transferId = getResponse.downloadId();
        final int lastBlock = (int) (fileInfo.size() / req.blockSize());

        final TransferProgressInfo progressInfo = new TransferProgressInfo(filePath, lastBlock);

        final TransferHandler handler;
        final QueueReader blockReader;
        handler = new FileDownloadHandler(filePath);
        blockReader = listener.incomingBlocksAsQueue(transferId, lastBlock);

        final TransferManager task = new TransferManager(
                transferId, handler, blockReader, progressInfo
        );

        transferInfoTable.put(transferId, progressInfo);
        transferPool.execute(task);

        return Optional.of(progressInfo);
    }

    public Optional<TransferProgressInfo> putFile(String localPath, String remotePath) throws IOException {
        final Path filePath = Path.of(localPath);
        final FileMetadata fileInfo;
        try {
            fileInfo = FileMetadata.fromPath(filePath);
        } catch (IOException e) {
            return Optional.empty();
        }

        final PUTRequest req = new PUTRequest(remotePath, fileInfo.size());
        serializeOut.writeObject(req);
        serializeOut.flush();

        PUTResponse putResponse = (PUTResponse) listener.receive();
        if(!putResponse.resultCode().equals(PUTResponse.Result.OK)) {
            return Optional.empty();
        }

        final int transferId = putResponse.uploadId();
        final int lastBlock = (int) (fileInfo.size() / req.blockSize());

        final TransferProgressInfo progressInfo = new TransferProgressInfo(filePath, lastBlock);

        final TransferHandler handler = new FileUploadHandler(serializeOut, filePath);
        final BlockSource fileReader = new FileBlockReader(filePath, transferId, req.blockSize());

        final TransferManager task = new TransferManager(
                transferId, handler, fileReader, progressInfo
        );

        transferInfoTable.put(transferId, progressInfo);
        transferPool.execute(task);

        return Optional.of(progressInfo);
    }

    public Stream<TransferProgressInfo> fetchTransferProgress() {
        return transferInfoTable.values().stream();
    }

    public void clearCancelledDownloads() {
        transferInfoTable.entrySet()
                .removeIf(e -> e.getValue().wasCancelled());
    }

    public void clearCompletedTransfers() {
        transferInfoTable.entrySet()
                .removeIf(e -> e.getValue().isComplete());
    }

    public void closeConnection() {
        transferPool.shutdown();

        try {
            if (!transferPool.awaitTermination(10, TimeUnit.SECONDS))
                transferPool.shutdown();
        } catch (InterruptedException e) {
            transferPool.shutdownNow();
        }

        try {
            listenerThread.interrupt();
            serializeOut.flush();
            serializeOut.close();
            server.close();
        } catch (IOException ignored) {
            // Exceptions are ignored since the connection will be closed regardless
        } finally {
            Thread.currentThread().interrupt();
        }
    }

    public boolean isCurrentDirAtStart() {
        return remotePath.isBlank();
    }

    public String getCurrentRemotePath() {
        return remotePath;
    }

    private void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    private String remotePath = "";
    private final BlockingObjectOutputStream serializeOut;
    private final Socket server;

    private final FileClientListener listener;
    private final Thread listenerThread;

    private final ConcurrentMap<Integer, TransferProgressInfo> transferInfoTable = new ConcurrentHashMap<>();
    private final ExecutorService transferPool = Executors.newFixedThreadPool(JFTPConstants.MAX_NUM_THREADS);
}