package school.dom.jftp.client;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.io.FileAccessMode;
import school.dom.jftp.common.io.TransferHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

public class DebugDownloadHandler implements TransferHandler {
    public DebugDownloadHandler(Path outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    @Override
    public boolean onTransferRegistered(int transferId) {
        System.out.printf("-- NEW DOWNLOAD REGISTERED --\nID: %d\n\n", downloadId);
        this.downloadId = transferId;
        return true;
    }

    @Override
    public void onTransferFinished() {
        System.out.printf("-- DOWNLOAD COMPLETED --\nID: %d\n\n", downloadId);
    }

    @Override
    public void onTransferCancelled() {
        System.out.printf("-- DOWNLOAD CANCELLED --\nID: %d\n\n", downloadId);
    }

    @Override
    public void onBlockProcessed(FileBlock block) {
        System.out.printf("-- BLOCK RECEIVED --\nID: %d\nSEQUENCE NUMBER: %d\n",
                block.operationId(), block.blockSeqNum());
        System.out.printf("%s\n", BufferToString(block.buffer()));
    }

    @Override
    public Path getTransferFilePath() {
        return outputFilePath;
    }

    @Override
    public FileAccessMode getTransferFileMode() {
        return null;
    }

    private static String BufferToString(byte[] buffer) {
        return new String(buffer, StandardCharsets.UTF_8);
    }

    private final Path outputFilePath;
    private int downloadId;
}
