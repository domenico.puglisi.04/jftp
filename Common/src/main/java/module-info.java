module school.dom.jftp.common {
    requires java.logging;
    exports school.dom.jftp.common.util;
    exports school.dom.jftp.common.io;
    exports school.dom.jftp.common.data;
}