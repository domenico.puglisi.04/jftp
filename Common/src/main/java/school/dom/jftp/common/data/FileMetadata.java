package school.dom.jftp.common.data;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Metadata transmitted from server to client to describe a single file.
 * Generally, metadata is encapsulated in a collection to describe an entire directory.
 * May be transmitted individually when client requests a specific file for download.
 */
public record FileMetadata(String name, FileType type, long size,
                           long lastModifiedTimestamp) implements Serializable, Comparable<FileMetadata> {
    // int hash; // may be expensive to compute; consider adding later

    public enum SizeUnit implements Comparable<SizeUnit> {
        B (1L),
        kB(1L << 10),
        MB(1L << 20),
        GB(1L << 30),
        TB(1L << 40); // <- hopefully people don't actually use this

        SizeUnit(long size) {
            this.size = size;
        }

        public long size() {
            return size;
        }

        public static SizeUnit getSizeScale(long size) {
            if (size < SizeUnit.kB.size())
                return SizeUnit.B;
            else if (size < SizeUnit.MB.size())
                return SizeUnit.kB;
            else if (size < SizeUnit.GB.size())
                return SizeUnit.MB;
            else if (size < SizeUnit.TB.size())
                return SizeUnit.GB;
            else
                return SizeUnit.TB;
        }

        public static double scaledSize(long size, SizeUnit order) {
            return (double) size / order.size();
        }

        private final long size;
    }

    public boolean isDir() {
        return type.equals(FileType.DIR);
    }

    public String getExtension() {
        final int i = name.lastIndexOf('.');
        if (i < 0 || isDir())
            return "";

        return name.substring(i);
    }

    public double scaledSize(SizeUnit order) {
        return SizeUnit.scaledSize(this.size, order);
    }

    public SizeUnit getSizeScale() {
        return SizeUnit.getSizeScale(this.size);
    }

    public LocalDateTime timestampDate() {
        return LocalDateTime.ofInstant(
                Instant.ofEpochMilli(lastModifiedTimestamp),
                ZoneOffset.systemDefault()
        );
    }

    @Override
    public int compareTo(FileMetadata other) {
        return this.name().compareToIgnoreCase(other.name());
    }

    public int compareToByExtension(FileMetadata other) {
        // first check if either file is a dir; if so, give dir priority
        if (this.isDir() && !other.isDir()) {
            return -1;
        } else if (!this.isDir() && other.isDir()) {
            return +1;
        }

        // then give priority to file extensions
        final int compareExtension = this.getExtension().compareToIgnoreCase(other.getExtension());
        if (compareExtension != 0)
            return compareExtension;

        // if extensions match, just compare file names
        return this.name().compareToIgnoreCase(other.name());
    }

    public static FileMetadata fromPath(Path filePath) throws IOException {
        var fileAttrs = Files.readAttributes(filePath, BasicFileAttributes.class);
        // Let's hope there's no symlinks or weird files mixed in ... !
        FileType fileType = (fileAttrs.isDirectory()) ? FileType.DIR : FileType.FILE;

        return new FileMetadata(
                filePath.getFileName().toString(),
                fileType,
                fileType.equals(FileType.DIR) ? 0 : fileAttrs.size(),
                fileAttrs.lastModifiedTime().toMillis()
        );
    }
}
