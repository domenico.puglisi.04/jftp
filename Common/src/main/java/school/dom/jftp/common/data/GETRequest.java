package school.dom.jftp.common.data;

import school.dom.jftp.common.util.JFTPConstants;

import java.io.Serializable;

/**
 * Class which describes a file download request submitted by a client to the server.
 *
 * @param filePath Path to the requested resource on the server.
 * @param startingBlockSeqNum When resuming a download, it is possible to specify a starting block number.
 * @param blockSize Maximum size to be used by each block during data transfer.
 */
public record GETRequest(String filePath, int startingBlockSeqNum, int blockSize) implements Serializable {
    public GETRequest(String filePath) {
        this(filePath, 0, JFTPConstants.DEFAULT_BLOCK_SIZE);
    }
}
