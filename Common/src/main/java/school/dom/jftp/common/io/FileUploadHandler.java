package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.util.BlockingObjectOutputStream;

import java.io.IOException;
import java.nio.file.Path;

import java.util.logging.Logger;

public class FileUploadHandler implements TransferHandler {
    public FileUploadHandler(BlockingObjectOutputStream uploadStream, Path inputFilePath) {
        this.uploadStream = uploadStream;
        this.inputFilePath = inputFilePath;
    }

    @Override
    public boolean onTransferRegistered(int uploadId) {
        this.uploadId = uploadId;
        LOGGER.fine(String.format("Starting upload for %s @ ID %d\n", inputFilePath, uploadId));
        return true;
    }

    @Override
    public void onTransferFinished() {
        LOGGER.fine(String.format("Finished upload for %s @ ID %d\n", inputFilePath, uploadId));
    }

    @Override
    public void onTransferCancelled() {
        LOGGER.fine(String.format("Cancelled upload for %s @ ID %d\n", inputFilePath, uploadId));
    }

    @Override
    public void onBlockProcessed(FileBlock block) throws IOException {
        uploadStream.reset();
        uploadStream.writeObject(block);
        uploadStream.flush();
    }

    @Override
    public Path getTransferFilePath() {
        return inputFilePath;
    }

    @Override
    public FileAccessMode getTransferFileMode() {
        return FileAccessMode.READ;
    }

    private final Path inputFilePath;
    private final BlockingObjectOutputStream uploadStream;
    private int uploadId;

    private final static Logger LOGGER = Logger.getAnonymousLogger();
}
