package school.dom.jftp.common.data;

import java.io.Serializable;
import java.util.HashSet;

/**
 * Class which describes the response sent to a client that communicated its Authorization Data to the server.
 *
 * @param resultCode Enum value which describes the result of the operation.
 * @param dirListing Metadata describing the root directory of the connection;
 *                   is guaranteed to be null when the result is negative.
 */
public record AuthorizationResponse(AuthorizationResponse.Result resultCode, HashSet<FileMetadata> dirListing,
                                    String startPath)
        implements Serializable {
    /**
     * Constructor to be used when generating a negative response, omitting root dir listing.
     */
    public AuthorizationResponse(AuthorizationResponse.Result resultCode) {
        this(resultCode, null, null);
    }

    /**
     * Holds the response code for the Authorization Response object.
     * May be extended with new values to report error conditions in greater detail.
     */
    public enum Result {
        OK,
        DENY
    }
}
