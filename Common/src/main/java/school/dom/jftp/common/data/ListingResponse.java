package school.dom.jftp.common.data;

import java.io.Serializable;
import java.util.HashSet;

/**
 * Class which describes the response sent to a client that requested the listing of a directory on the server.
 *
 * @param resultCode Enum value which describes the result of the operation.
 * @param dirListing Metadata describing the directory specified by the request;
 *                   is guaranteed to be null when the result is negative.
 */
public record ListingResponse(ListingResponse.Result resultCode, HashSet<FileMetadata> dirListing, String dirPath)
        implements Serializable {
    /**
     * Constructor to be used when generating a negative response, omitting dir listing.
     */
    public ListingResponse(ListingResponse.Result resultCode) {
        this(resultCode, null, null);
    }

    /**
     * Holds the response code for the Listing Response object.
     * May be extended with new values to report error conditions in greater detail.
     */
    public enum Result {
        OK,
        NONEXISTENT,
        DENY
    }
}
