package school.dom.jftp.common.data;

import java.io.Serializable;

/**
 * Class which describes the response sent to a client that requested to send a resource to the server.
 *
 * @param resultCode Enum value which describes the result of the operation.
 * @param uploadId Identifier to be used by both client and server to refer to the PUT operation.
 */
public record PUTResponse(PUTResponse.Result resultCode, Integer uploadId) implements Serializable {
    /**
     * Constructor to be used when generating a negative response, omitting the upload id.
     */
    public PUTResponse(PUTResponse.Result resultCode) {
        this(resultCode, null);
    }

    /**
     * Holds the response code for the PUT Response object.
     * May be extended with new values to report error conditions in greater detail.
     */
    public enum Result {
        OK,
        DENY
    }
}
