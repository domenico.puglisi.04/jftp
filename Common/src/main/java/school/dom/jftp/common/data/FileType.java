package school.dom.jftp.common.data;

/**
 * Simply describes whether a file is a directory or a regular binary file.
 * May be extended to support different file types in the future (with questionable usefulness).
 */
public enum FileType {
    DIR,
    FILE,
}
