package school.dom.jftp.common.io;

public enum FileAccessMode {
    READ,
    WRITE,
}
