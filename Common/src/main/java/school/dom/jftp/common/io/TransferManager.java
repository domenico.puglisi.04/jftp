package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.data.TransferProgressInfo;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

public class TransferManager implements Runnable {
    public TransferManager(int transferId, TransferHandler handler, BlockSource blockSource) {
        this.transferId = transferId;
        this.blockSource = blockSource;
        this.handler = handler;
        this.progressStatus = Optional.empty();
    }

    public TransferManager(int transferId, TransferHandler handler, BlockSource blockSource,
                           TransferProgressInfo progressStatus) {
        this.transferId = transferId;
        this.blockSource = blockSource;
        this.handler = handler;
        this.progressStatus = Optional.of(progressStatus);
    }

    @Override
    public void run() {
        final Path transferFilePath = handler.getTransferFilePath();
        final FileAccessMode transferFileMode = handler.getTransferFileMode();
        final int lastBlockNum = blockSource.getLastBlockSequenceNumber();

        if (!acquiredResources && !acquireResources()) {
            progressStatus.ifPresent(TransferProgressInfo::cancelCount);
            return;
        }

        if (!handler.onTransferRegistered(transferId)) {
            releaseResources();
            progressStatus.ifPresent(TransferProgressInfo::cancelCount);
            return;
        }

        while (true) {
            FileBlock toProcess = blockSource.get();
            if (toProcess == null)
                break;

            // should be logged...
            if (toProcess.operationId() != transferId)
                continue;

            try {
                handler.onBlockProcessed(toProcess);
                progressStatus.ifPresent(TransferProgressInfo::incrementBlockCount);
            } catch (IOException e) {
                e.printStackTrace();
                handler.onTransferCancelled();
                progressStatus.ifPresent(TransferProgressInfo::cancelCount);
                releaseResources();
                return;
            }

            if (toProcess.blockSeqNum() == lastBlockNum) {
                progressStatus.ifPresent(TransferProgressInfo::setComplete);
                break;
            }
        }

        handler.onTransferFinished();
        releaseResources();
    }

    public boolean acquireResources() {
        var retval = fileAccessor.requestFileAccess(handler.getTransferFileMode(), handler.getTransferFilePath());
        if (retval)
            acquiredResources = true;
        return retval;
    }

    private void releaseResources() {
        fileAccessor.releaseFileAccess(handler.getTransferFileMode(), handler.getTransferFilePath());
    }

    private final int transferId;
    private final BlockSource blockSource;
    private final TransferHandler handler;
    private final Optional<TransferProgressInfo> progressStatus;

    private boolean acquiredResources = false;

    private static final LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
}
