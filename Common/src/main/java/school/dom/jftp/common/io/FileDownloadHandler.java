package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.data.TransferProgressInfo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class FileDownloadHandler implements TransferHandler {
    public FileDownloadHandler(Path outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    @Override
    public boolean onTransferRegistered(int transferId) {
        try {
            outputFileStream = new FileOutputStream(String.valueOf(outputFilePath));
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    @Override
    public void onTransferFinished() {
        try {
            outputFileStream.close();
        } catch (IOException ignored) {
            // file needs to be closed anyway
        }
    }

    @Override
    public void onTransferCancelled() {
        try {
            outputFileStream.close();
        } catch (IOException ignored) {
            // file needs to be closed anyway
        }
    }

    @Override
    public void onBlockProcessed(FileBlock block) throws IOException {
        outputFileStream.write(block.buffer());
    }

    @Override
    public Path getTransferFilePath() {
        return outputFilePath;
    }

    @Override
    public FileAccessMode getTransferFileMode() {
        return FileAccessMode.WRITE;
    }

    private final Path outputFilePath;
    private FileOutputStream outputFileStream;
}
