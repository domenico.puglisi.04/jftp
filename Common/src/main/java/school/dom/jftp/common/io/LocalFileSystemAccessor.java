package school.dom.jftp.common.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class LocalFileSystemAccessor {
    public static LocalFileSystemAccessor getAccessor() {
        return singleton;
    }

    private final static LocalFileSystemAccessor singleton = new LocalFileSystemAccessor();
    private LocalFileSystemAccessor() {}

    public boolean whitelistPath(Path toRegister) {
        try {
            toRegister = toRegister.toRealPath();
        } catch (IOException e) {
            System.err.println("Path to " + toRegister + " does not exist");
            return false;
        }

        final Path finalToRegister = toRegister;
        if(dirWhitelist.stream().noneMatch(finalToRegister::startsWith)) {
            dirWhitelist.add(finalToRegister);
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean requestFileAccess(FileAccessMode accessMode, Path requestedFilePath) {
        LOGGER.fine("Requesting " + requestedFilePath + " for " + accessMode);
        switch (accessMode) {
            case READ -> {
                return requestFileRead(requestedFilePath);
            }
            case WRITE -> {
                return requestFileWrite(requestedFilePath);
            }

            default -> {
                // should log this
                return false;
            }
        }
    }

    public synchronized void releaseFileAccess(FileAccessMode accessMode, Path releasedFilePath) {
        LOGGER.fine("Releasing " + releasedFilePath + " for " + accessMode);
        switch (accessMode) {
            case READ -> releaseFileRead(releasedFilePath);
            case WRITE -> releaseFileWrite(releasedFilePath);
        }
    }

    public synchronized boolean requestFileRead(Path requestedFilePath) {
        requestedFilePath = normalizePath(requestedFilePath);

        if(!isPathInWhitelist(requestedFilePath))
            return false;

        FileAccessInfo requestedFileInfo = fileAccessMap.get(requestedFilePath);
        if((requestedFileInfo != null && !requestedFileInfo.isAvailableForReading())
                || !Files.exists(requestedFilePath) || Files.isDirectory(requestedFilePath)) {
            return false;
        } else {
            fileAccessMap.putIfAbsent(requestedFilePath, new FileAccessInfo(FileAccessMode.READ));
            requestedFileInfo = fileAccessMap.get(requestedFilePath);
            requestedFileInfo.incrementUsageCount();
            return true;
        }
    }

    public synchronized boolean requestFileWrite(Path requestedFilePath) {
        requestedFilePath = normalizePath(requestedFilePath);

        if(!isPathInWhitelist(requestedFilePath))
            return false;

        if(fileAccessMap.containsKey(requestedFilePath) || Files.isDirectory(requestedFilePath))
            return false;

        FileAccessInfo requestedFileInfo =
                fileAccessMap.putIfAbsent(requestedFilePath, new FileAccessInfo(FileAccessMode.WRITE));

        if (requestedFileInfo == null)
            requestedFileInfo = fileAccessMap.get(requestedFilePath);
        requestedFileInfo.incrementUsageCount();
        return true;
    }

    public synchronized void releaseFileRead(Path releasedFilePath) {
        releasedFilePath = normalizePath(releasedFilePath);

        FileAccessInfo releasedFileInfo = fileAccessMap.get(releasedFilePath);
        if (releasedFileInfo != null && releasedFileInfo.getAccessMode().equals(FileAccessMode.READ))
            releasedFileInfo.decrementUsageCount();
        else
            LOGGER.warning("Trying to release file that was not accessed previously");

        if(releasedFileInfo != null && releasedFileInfo.getUsageCount() == 0)
            fileAccessMap.remove(releasedFilePath);
    }

    public synchronized void releaseFileWrite(Path releasedFilePath) {
        releasedFilePath = normalizePath(releasedFilePath);

        FileAccessInfo releasedFileInfo = fileAccessMap.get(releasedFilePath);
        assert releasedFileInfo != null && releasedFileInfo.getAccessMode().equals(FileAccessMode.WRITE);
        releasedFileInfo.decrementUsageCount();
        assert releasedFileInfo.getUsageCount() == 0;
        fileAccessMap.remove(releasedFilePath);
    }

    public synchronized boolean isPathInWhitelist(Path requestedFilePath) {
        return dirWhitelist.stream().anyMatch(requestedFilePath::startsWith);
    }

    public synchronized boolean isPathAvailableForReading(Path requestedFilePath) {
        if(!fileAccessMap.containsKey(requestedFilePath))
            return true;

        return fileAccessMap.get(requestedFilePath).isAvailableForReading();
    }

    private Path normalizePath(Path p) {
        return p.toAbsolutePath().normalize();
    }

    private final Map<Path, FileAccessInfo> fileAccessMap = new HashMap<>();
    private final Set<Path> dirWhitelist = new HashSet<>();

    private static class FileAccessInfo {
        public FileAccessInfo(FileAccessMode accessMode) {
            this.accessMode = accessMode;
        }

        public boolean isAvailableForReading() {
            return !accessMode.equals(FileAccessMode.WRITE);
        }

        public FileAccessMode getAccessMode() {
            return accessMode;
        }

        public void setAccessMode(FileAccessMode accessMode) {
            this.accessMode = accessMode;
        }

        public int getUsageCount() {
            return usageCount;
        }

        public void incrementUsageCount() {
            usageCount++;
        }

        public void decrementUsageCount() {
            usageCount--;
        }

        private FileAccessMode accessMode;
        private int usageCount = 0;
    }

    private static final Logger LOGGER = Logger.getAnonymousLogger();
}

