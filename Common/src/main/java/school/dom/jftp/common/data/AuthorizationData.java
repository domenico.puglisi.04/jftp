package school.dom.jftp.common.data;

import java.io.Serializable;

/**
 * Contains only username and plaintext password communicated during the authentication phase.
 * Password remains unencrypted as it is checked against its hash server-side;
 * ideally, the JFTP connection itself would be made secure through SSL.
 */
public record AuthorizationData(String username, String password) implements Serializable {}
