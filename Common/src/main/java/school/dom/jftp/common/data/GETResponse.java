package school.dom.jftp.common.data;

import java.io.Serializable;

/**
 * Class which describes the response sent to a client that requested a resource on the server.
 *
 * @param resultCode Enum value which describes the result of the operation.
 * @param fileInfo Metadata of the requested resource; is guaranteed to be null when the result is negative.
 * @param downloadId Identifier to be used by both client and server to refer to the GET operation.
 */
public record GETResponse(GETResponse.Result resultCode, FileMetadata fileInfo, Integer downloadId, Integer blockSize)
        implements Serializable {
    /**
     * Constructor to be used when generating a negative response, omitting file metadata and download id.
     */
    public GETResponse(GETResponse.Result resultCode) {
        this(resultCode, null, null, null);
    }

    /**
     * Holds the response code for the GET Response object.
     * May be extended with new values to report error conditions in greater detail.
     */
    public enum Result {
        OK,
        DENY
    }
}
