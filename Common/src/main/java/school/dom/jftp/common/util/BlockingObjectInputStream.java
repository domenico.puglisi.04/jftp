package school.dom.jftp.common.util;

import java.io.*;

public class BlockingObjectInputStream {
    public BlockingObjectInputStream(InputStream in) throws IOException {
        this.in = new ObjectInputStream(in);
    }

    public synchronized Object readObject() throws IOException, ClassNotFoundException {
        return in.readObject();
    }

    public synchronized void close() throws IOException {
        in.close();
    }

    private final ObjectInputStream in;
}
