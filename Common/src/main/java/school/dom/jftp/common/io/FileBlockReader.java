package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;
import school.dom.jftp.common.util.JFTPConstants;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class FileBlockReader implements BlockSource {
    public FileBlockReader(Path inputFilePath, int operationId, int blockSize)
            throws IOException {
        if (Files.isDirectory(inputFilePath))
            throw new IOException("Cannot open directory as a file");

        inputFileStream = new FileInputStream(String.valueOf(inputFilePath));
        long fileSize = Files.size(inputFilePath);

        lastBlockNum = (int) (fileSize / blockSize);
        this.inputFilePath = inputFilePath;
        this.operationId = operationId;
        this.blockSize = blockSize;
        this.buffer = new byte[blockSize];
    }

    public FileBlockReader(Path inputFilePath, int operationId) throws IOException {
        this(inputFilePath, operationId, JFTPConstants.DEFAULT_BLOCK_SIZE);
    }

    @Override
    public FileBlock get() {
        final int bytesRead;
        try {
            bytesRead = inputFileStream.read(buffer);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }

        if(seqNum == lastBlockNum) {
            try {
                inputFileStream.close();
            } catch (IOException ignored) {
                return null;
            }

            if (bytesRead == -1) {
                return null;
            }
        }

        // We want to get a resized (shrunk) copy of the buffer to avoid writing extra bytes
        if (bytesRead < blockSize) {
            byte[] toTransfer = Arrays.copyOf(buffer, bytesRead);
            return new FileBlock(operationId, seqNum++, toTransfer);
        } else {
            return new FileBlock(operationId, seqNum++, buffer);
        }
    }

    @Override
    public int getLastBlockSequenceNumber() {
        return lastBlockNum;
    }

    private final Path inputFilePath;
    private final FileInputStream inputFileStream;

    private final int blockSize;
    private final int operationId;
    private final int lastBlockNum;
    private int seqNum = 0;

    private final byte[] buffer;

    private static final LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
}
