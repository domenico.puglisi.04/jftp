package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;

import java.util.function.Supplier;

public interface BlockSource extends Supplier<FileBlock> {
    int getLastBlockSequenceNumber();
}
