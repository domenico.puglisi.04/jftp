package school.dom.jftp.common.data;

import java.io.Serializable;

/**
 * Block which encapsulates a portion of a file's contents.
 * @param operationId The identifier associated with this block's transfer operation.
 * @param blockSeqNum Sequence number of the block within the whole file contents.
 * @param buffer The byte buffer containing the actual contents of the block being transferred.
 */
public record FileBlock(int operationId, int blockSeqNum, byte[] buffer) implements Serializable {
    @Override
    public String toString() {
        return "FileBlock{" +
                "operationId=" + operationId +
                ", blockSeqNum=" + blockSeqNum +
                '}';
    }
}
