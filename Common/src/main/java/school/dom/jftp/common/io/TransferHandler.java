package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;

import java.io.IOException;
import java.nio.file.Path;

public interface TransferHandler {
    boolean onTransferRegistered(int transferId);
    void    onTransferFinished();
    void    onTransferCancelled();
    void    onBlockProcessed(FileBlock block) throws IOException;

    Path           getTransferFilePath();
    FileAccessMode getTransferFileMode();
}
