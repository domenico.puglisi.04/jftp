package school.dom.jftp.common.data;

import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;

public class TransferProgressInfo {
    public TransferProgressInfo(Path filePath, int lastBlockNum) {
        this.lastBlockNum = lastBlockNum;
        this.filePath = filePath;
    }

    public int getLastBlockNum() {
        return lastBlockNum;
    }

    public int getBlocksProcessed() {
        return blocksProcessed.get();
    }

    public Path getFilePath() {
        return filePath;
    }

    public void incrementBlockCount() {
        assert blocksProcessed.get() != -1;
        blocksProcessed.getAndIncrement();
    }

    public double getCompletionPercent() {
        return (double) blocksProcessed.get() / (double) (lastBlockNum + 1);
    }

    public boolean wasCancelled() {
        return blocksProcessed.get() == -1;
    }
    public boolean isComplete() {
        // we increment lastBlockNum by one as block numbering starts at 0
        return blocksProcessed.get() >= lastBlockNum + 1;
    }

    public void setComplete() {
        blocksProcessed.set(lastBlockNum + 1);
    }

    public void cancelCount() {
        blocksProcessed.set(-1);
    }

    @Override
    public String toString() {
        return "TransferProgressInfo{" +
                "blocksProcessed=" + blocksProcessed +
                ", lastBlockNum=" + lastBlockNum +
                ", filePath=" + filePath +
                '}';
    }

    public String transferInfoAsString() {
        if (blocksProcessed.get() == -1)
            return String.format("FILE: %s [OPERATION CANCELLED]", filePath);
        else
            return String.format("FILE: %s %3.2f%%", filePath, getCompletionPercent() * 100.0);
    }

    private final AtomicInteger blocksProcessed = new AtomicInteger(0);
    private final int lastBlockNum;
    private final Path filePath;
}
