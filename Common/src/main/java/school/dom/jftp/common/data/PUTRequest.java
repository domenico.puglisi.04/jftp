package school.dom.jftp.common.data;

import school.dom.jftp.common.util.JFTPConstants;

import java.io.Serializable;

/**
 * Class which describes a file upload request submitted by a client to the server.
 *
 * @param newFilePath Path to the resource to be created or overwritten on the server.
 * @param fileSize Size (in Bytes) of the file to be transmitted to the server.
 * @param blockSize Maximum size to be used by each block during data transfer.
 */
public record PUTRequest(String newFilePath, long fileSize, int blockSize) implements Serializable {
    public PUTRequest(String newFilePath, long fileSize) {
        this(newFilePath, fileSize, JFTPConstants.DEFAULT_BLOCK_SIZE);
    }
}
