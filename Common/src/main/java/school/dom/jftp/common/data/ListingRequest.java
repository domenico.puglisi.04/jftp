package school.dom.jftp.common.data;

import java.io.Serializable;

/**
 * Class used by clients to request dir metadata.
 *
 * @param dirPath Path to the directory whose listing was requested by a client.
 */
public record ListingRequest(String dirPath) implements Serializable {}
