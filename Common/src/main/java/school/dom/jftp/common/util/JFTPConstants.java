package school.dom.jftp.common.util;

import java.time.format.DateTimeFormatter;

public class JFTPConstants {
    public static final int DEFAULT_BLOCK_SIZE = 1024;
    public static final int DEFAULT_SERVER_PORT = 3663;

    public static final int MAX_NUM_THREADS = 3;
    public static final String CONFIG_NO_PASSWORD = "default";

    public static final DateTimeFormatter TIMESTAMP_FORMATTER
            = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");
}
