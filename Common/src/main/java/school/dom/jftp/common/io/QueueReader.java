package school.dom.jftp.common.io;

import school.dom.jftp.common.data.FileBlock;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class QueueReader implements BlockSource {
    public QueueReader(BlockingQueue<FileBlock> blocksToProcess, int lastBlockNum) {
        this.blocksToProcess = blocksToProcess;
        this.lastBlockNum = lastBlockNum;
    }

    @Override
    public FileBlock get() {
        FileBlock retval;
        try {
            retval = blocksToProcess.poll(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            return null;
        }

        return retval;
    }

    @Override
    public int getLastBlockSequenceNumber() {
        return lastBlockNum;
    }

    private final int lastBlockNum;
    private final BlockingQueue<FileBlock> blocksToProcess;
}
