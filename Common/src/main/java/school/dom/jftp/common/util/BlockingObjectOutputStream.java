package school.dom.jftp.common.util;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class BlockingObjectOutputStream {
    public BlockingObjectOutputStream(OutputStream out) throws IOException {
        this.out = new ObjectOutputStream(out);
    }

    public synchronized void writeObject(Object obj) throws IOException {
        out.writeObject(obj);
    }

    public synchronized void flush() throws IOException {
        out.flush();
    }

    public synchronized void close() throws IOException {
        out.close();
    }

    public synchronized void reset() throws IOException {
        out.reset();
    }

    private final ObjectOutputStream out;
}
