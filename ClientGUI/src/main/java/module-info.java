module school.dom.jftp.clientgui {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.ikonli.javafx;

    requires school.dom.jftp.common;
    requires school.dom.jftp.client;

    opens school.dom.jftp.clientgui to javafx.fxml;
    exports school.dom.jftp.clientgui;
}