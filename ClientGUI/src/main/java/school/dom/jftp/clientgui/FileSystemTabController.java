package school.dom.jftp.clientgui;

import javafx.fxml.FXML;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import school.dom.jftp.common.data.FileMetadata;
import school.dom.jftp.common.data.FileType;

import java.time.LocalDateTime;

public class FileSystemTabController {

    public void onStartup() {
        // displayTable.getSortOrder().add(typeColumn);
        // displayTable.getSortOrder().add(sizeColumn);
        // sizeColumn.setCellFactory(FileTableEntry.SIZE_DISPLAY);
    }

    @FXML
    public void onRemoteFileClick(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() < 2)
            mouseEvent.consume();
    }
}
