package school.dom.jftp.clientgui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import school.dom.jftp.common.io.LocalFileSystemAccessor;

import java.io.IOException;
import java.nio.file.Path;

public class ClientGUI extends Application {
    @Override
    public void start(Stage stage) {
        fileAccessor.whitelistPath(Path.of(System.getProperty("user.dir")));
        final FXMLLoader fxmlLoader = new FXMLLoader(ClientGUI.class.getResource("jftp-client-view.fxml"));
        final Parent root;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        controller = fxmlLoader.getController();
        controller.onStartup();

        final Scene scene = new Scene(root);
        stage.setTitle("JFTP GUI Client");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        controller.onConnectionClose();
        super.stop();
    }

    public static void main(String[] args) {
        launch();
    }

    public GUIController controller;
    public static final LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
}
