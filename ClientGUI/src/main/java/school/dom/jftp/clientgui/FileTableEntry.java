package school.dom.jftp.clientgui;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.adapter.JavaBeanObjectProperty;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.util.Callback;
import school.dom.jftp.common.data.FileMetadata;
import school.dom.jftp.common.data.FileType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.kordamp.ikonli.javafx.FontIcon;

public class FileTableEntry {
    public FileTableEntry(FileMetadata fileInfo) {
        this.fileInfo = fileInfo;
    }

    public String getName() {
        return fileInfo.name();
    }

    public FileType getType() {
        return fileInfo.type();
    }

    public Long getSize() {
        return fileInfo.size();
    }

    public LocalDateTime getLastModified() {
        return fileInfo.timestampDate();
    }

    private final FileMetadata fileInfo;

    @FXML
    public static final
    Callback<
            TreeTableColumn<FileTableEntry, String>, TreeTableCell<FileTableEntry, String>
            > NAME_DISPLAY = new Callback<>() {
        @Override
        public TreeTableCell<FileTableEntry, String> call(TreeTableColumn<FileTableEntry, String> column) {
            return new FileNameCell();
        }
    };

    @FXML
    public static final
    Callback<
            TreeTableColumn<FileTableEntry, Long>, TreeTableCell<FileTableEntry, Long>
            > SIZE_DISPLAY = new Callback<>() {
        @Override
        public TreeTableCell<FileTableEntry, Long> call(TreeTableColumn<FileTableEntry, Long> column) {
            return new FileSizeCell();
        }
    };

    @FXML
    public static final
    Callback<
            TreeTableColumn<FileTableEntry, FileType>, TreeTableCell<FileTableEntry, FileType>
            > TYPE_DISPLAY = new Callback<>() {
        @Override
        public TreeTableCell<FileTableEntry, FileType>
        call(TreeTableColumn<FileTableEntry, FileType> column) {
            return new FileTypeCell();
        }
    };

    @FXML
    public static final
    Callback<
        TreeTableColumn<FileTableEntry, LocalDateTime>, TreeTableCell<FileTableEntry, LocalDateTime>
    > DATE_DISPLAY = new Callback<>() {
        @Override
        public TreeTableCell<FileTableEntry, LocalDateTime>
        call(TreeTableColumn<FileTableEntry, LocalDateTime> column) {
            return new FileTimeCell();
        }
    };

    public static class FileSizeCell extends TreeTableCell<FileTableEntry, Long> {
        @Override
        protected void updateItem(Long size, boolean empty) {
            super.updateItem(size, empty);
            if (size == null || size == 0) {
                setText("");
            } else {
                final var order = FileMetadata.SizeUnit.getSizeScale(size);
                final var scaledSize = FileMetadata.SizeUnit.scaledSize(size, order);
                setText(String.format("%5.1f %s", scaledSize, order));
            }
        }
    }

    public static class FileTimeCell extends TreeTableCell<FileTableEntry, LocalDateTime> {
        @Override
        protected void updateItem(LocalDateTime timestamp, boolean empty) {
            super.updateItem(timestamp, empty);
            if (timestamp == null || timestamp.toLocalDate().equals(LocalDate.EPOCH)) {
                setText("");
            } else {
                setText(dateFmt.format(timestamp));
            }
        }

        private static final DateTimeFormatter dateFmt = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
    }

    public static class FileNameCell extends TreeTableCell<FileTableEntry, String> {
        @Override
        protected void updateItem(String name, boolean empty) {
            super.updateItem(name, empty);

            final TreeItem<FileTableEntry> i = getTableRow().getTreeItem();
            if (i == null) {
                setText(null);
                setGraphic(null);
                return;
            }

            final FileTableEntry e = i.getValue();
            switch (e.getType()) {
                case DIR -> {
                    if (i.isExpanded())
                        setGraphic(expandedFolderIcon);
                    else
                        setGraphic(regularFolderIcon);
                }
                case FILE, null -> setGraphic(null);
            }

            setText(name);
        }

        private final FontIcon regularFolderIcon = new FontIcon("codicon-folder:16");
        private final FontIcon expandedFolderIcon = new FontIcon("codicon-folder-opened:16");
        // private final FontIcon fileIcon = new FontIcon("codicon-file:16");
    }

    public static class FileTypeCell extends TreeTableCell<FileTableEntry, FileType> {
        @Override
        protected void updateItem(FileType type, boolean empty) {
            super.updateItem(type, empty);
            switch (type) {
                case DIR  -> setGraphic(folderIcon);
                case FILE -> setGraphic(fileIcon);
                case null -> setGraphic(null);
            }
        }

        private final FontIcon folderIcon = new FontIcon("codicon-folder:16");
        private final FontIcon fileIcon   = new FontIcon("codicon-file:16");
    }
}
