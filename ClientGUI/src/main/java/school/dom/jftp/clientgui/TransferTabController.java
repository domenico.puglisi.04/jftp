package school.dom.jftp.clientgui;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import school.dom.jftp.common.data.TransferProgressInfo;

public class TransferTabController {
    @FXML public VBox transferList;

    public void addNewTransfer(TransferProgressInfo p) {
        Label name = new Label(p.getFilePath().toString());
        ProgressBar bar = new ProgressBar();
        HBox progressInfoContainer = new HBox();
        progressInfoContainer.getChildren().addAll(bar, name);
        progressInfoContainer.setSpacing(10.0);
        progressInfoContainer.setPadding(new Insets(10.0));
        progressInfoContainer.setPrefHeight(20.0);

        FileTransferTask transferTask = new FileTransferTask(p);
        bar.progressProperty().bind(transferTask.progressProperty());

        transferList.getChildren().add(progressInfoContainer);
        Thread t = new Thread(transferTask);
        t.start();
    }
}
