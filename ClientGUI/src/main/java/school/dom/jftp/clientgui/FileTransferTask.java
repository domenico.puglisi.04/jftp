package school.dom.jftp.clientgui;

import javafx.concurrent.Task;
import school.dom.jftp.common.data.TransferProgressInfo;

public class FileTransferTask extends Task<Integer> {
    public FileTransferTask(TransferProgressInfo progressInfo) {
        this.progressInfo = progressInfo;
    }

    @Override
    public Integer call() {
        final int lastBlockNum = progressInfo.getLastBlockNum();
        updateMessage("Transfer started");
        updateProgress(0, lastBlockNum+1);

        while (!progressInfo.isComplete()) {
            final int progress = progressInfo.getBlocksProcessed();
            if (isCancelled() || progress == -1) {
                updateMessage("Transfer cancelled");
                return progress;
            }

            updateMessage("Obtained " + progress + " blocks out of " + lastBlockNum);
            updateProgress(progress, lastBlockNum);

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                if (isCancelled()) {
                    updateMessage("Transfer Progress");
                    return progress;
                }
            }
        }

        updateProgress(lastBlockNum+1, lastBlockNum+1);
        return lastBlockNum;
    }

    private final TransferProgressInfo progressInfo;
}
