package school.dom.jftp.clientgui;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import school.dom.jftp.client.FileClient;
import school.dom.jftp.common.data.*;
import school.dom.jftp.common.io.*;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.stage.DirectoryChooser;

import javafx.fxml.FXML;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.*;

public class GUIController {
    @FXML public Node rootNode;

    // General UI
    @FXML public TextField hostnameField;
    @FXML public TextField portField;
    @FXML public TextField usernameField;
    @FXML public PasswordField passwordField;

    @FXML public Button connectButton;
    @FXML public Button disconnectButton;
    @FXML public Button downloadButton;
    @FXML public Button uploadButton;

    // Filesystem Explorer
    @FXML public TreeTableView<FileTableEntry> displayTable;
    @FXML public TreeTableColumn<FileTableEntry, String> nameColumn;
    @FXML public TreeTableColumn<FileTableEntry, FileType> typeColumn;
    @FXML public TreeTableColumn<FileTableEntry, Long> sizeColumn;
    @FXML public TreeTableColumn<FileTableEntry, LocalDateTime> timestampColumn;

    // Transfer Status
    @FXML public VBox transferList;


    public void onStartup() {}

    @FXML
    protected void onConnectionStart() {
        onConnectionClose();

        connectButton.setDisable(true);
        disconnectButton.setDisable(false);
        uploadButton.setDisable(false);
        downloadButton.setDisable(false);

        hostnameField.setDisable(true);
        portField.setDisable(true);
        usernameField.setDisable(true);
        passwordField.setDisable(true);

        final String hostname = hostnameField.getText();
        final int port = Integer.parseInt(portField.getText());
        final String username = usernameField.getText();
        final String password = passwordField.getText();

        assert password != null;

        try {
            Socket server = new Socket(hostname, port);
            this.client = new FileClient(server);
        } catch (IOException e) {
            System.err.println("ERROR: " + e);
            onConnectionClose();
            return;
        }

        try {
            user = new AuthorizationData(username, password);
            status = client.authenticate(user);
            if (!status.resultCode().equals(AuthorizationResponse.Result.OK)) {
                System.err.println("ERROR: invalid credentials, server denied connection.");
                onConnectionClose();
                return;
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("ERROR: " + e);
            onConnectionClose();
            return;
        }

        rootFile = new TreeItem<>(new FileTableEntry(
                new FileMetadata(status.startPath(), FileType.DIR, 0, 0)
        ));

        rootFile.addEventHandler(
            TreeItem.expandedItemCountChangeEvent(),
            (EventHandler<TreeItem.TreeModificationEvent<FileTableEntry>>) event -> {
                displayTable.refresh();
            }
        );

        onStartup();
        expandListing(status.startPath(), status.dirListing(), rootFile);
        displayTable.setRoot(rootFile);
        displayTable.refresh();
    }

    protected void expandListing(String dir, HashSet<FileMetadata> dirListing, TreeItem<FileTableEntry> parent) {
        for (var file : dirListing) {
            if (file.isDir()) {
                String innerPath = dir + "/" + file.name();
                final ListingResponse res;
                try {
                    final ListingRequest req = new ListingRequest(innerPath);
                    res = client.getDirectoryListing(req);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                    return;
                }

                if(!res.resultCode().equals(ListingResponse.Result.OK))
                    return;

                var child = new TreeItem<>(new FileTableEntry(file));
                parent.getChildren().add(child);
                expandListing(innerPath, res.dirListing(), child);
            } else {
                final var child = new TreeItem<>(new FileTableEntry(file));
                parent.getChildren().add(child);
            }
        }
    }

    @FXML
    public void onRefreshListing() {
        var temp = new TreeItem<>(new FileTableEntry(
                new FileMetadata(status.startPath(), FileType.DIR, 0, 0)
        ));

        final ListingResponse ls;
        try {
            ls = client.getDirectoryListing(new ListingRequest(status.startPath()));
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("ERROR: " + e);
            onConnectionClose();
            return;
        }

        if (!ls.resultCode().equals(ListingResponse.Result.OK))
            return;

        expandListing(ls.dirPath(), ls.dirListing(), temp);
        rootFile = temp;
        displayTable.setRoot(rootFile);
        displayTable.sort();
        displayTable.refresh();
    }

    @FXML
    public void onRemoteFileClick(MouseEvent event) {
        if (event.getClickCount() == 2)
            onFileDownload();
    }

    @FXML
    public void onDownloadButtonClick() {
        onFileDownload();
    }
    
    public void onFileDownload() {
        String remotePath = resolveSelectedFilePath(null);

        final TransferProgressInfo progressInfo;
        try {
            var p = client.getFile(remotePath);
            if (p.isEmpty()) {
                System.err.println("ERROR: Could not initiate file download.");
                return;
            } else {
                progressInfo = p.get();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        addNewTransfer(progressInfo);
    }

    public String resolveSelectedFilePath(String toAppend) {
        final TreeItem<FileTableEntry> item = displayTable.getSelectionModel().getSelectedItem();
        if (item == null)
            return "";

        final FileTableEntry entry = item.getValue();
        final Deque<String> parentFolderNames = new LinkedList<>();
        for (var p = item.getParent();
             p != null; p = p.getParent()) {
            parentFolderNames.addFirst(p.getValue().getName());
        }

        StringBuilder resultingPath = new StringBuilder();
        for (var n : parentFolderNames) {
            resultingPath.append(n);
            resultingPath.append('/');
        }

        resultingPath.append(entry.getName());
        if (toAppend != null) {
            resultingPath.append('/');
            resultingPath.append(toAppend);
        }
        return resultingPath.toString();
    }

    @FXML
    protected void onDirectoryAdd() {
        DirectoryChooser uploadDirAdd = new DirectoryChooser();
        uploadDirAdd.setInitialDirectory(new File(String.valueOf(rootDir)));
        File dir = uploadDirAdd.showDialog(rootNode.getScene().getWindow());

        if (dir != null) {
            if (fileAccessor.whitelistPath(dir.toPath()))
                System.out.println("INFO: Whitelisted path to file successfully: " + dir.getPath());
            else
                System.out.println("WARNING: Path could not be whitelisted: " + dir.getPath());
        }
    }

    @FXML
    protected void onFileChoose() {
        FileChooser uploadFileAdd = new FileChooser();
        uploadFileAdd.setInitialDirectory(new File(String.valueOf(rootDir)));
        File f = uploadFileAdd.showOpenDialog(rootNode.getScene().getWindow());

        if (f == null)
            return;

        final Path dirToFile = f.getParentFile().toPath();
        if (!fileAccessor.isPathInWhitelist(dirToFile)) {
            if (fileAccessor.whitelistPath(dirToFile)) {
                System.out.println("INFO: Whitelisted path to file successfully: " + f.getParentFile().getPath());
            } else {
                System.err.println("WARNING: Could not whitelist path to file: " + f.getParentFile().getPath());
                return;
            }
        } else {
            System.out.println("INFO: Path was already in whitelist: " + f.getParentFile().getPath());
        }

        String remotePath = resolveSelectedFilePath(f.getName());

        final TransferProgressInfo progressInfo;
        try {
            var p = client.putFile(f.getPath(), remotePath);
            if (p.isEmpty()) {
                System.err.println("ERROR: Could not initiate file upload.");
                return;
            } else {
                progressInfo = p.get();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        addNewTransfer(progressInfo);
    }

    public void addNewTransfer(TransferProgressInfo p) {
        Label name = new Label(p.getFilePath().toString());
        ProgressBar bar = new ProgressBar();
        HBox progressInfoContainer = new HBox();
        progressInfoContainer.getChildren().addAll(bar, name);
        progressInfoContainer.setSpacing(10.0);
        progressInfoContainer.setPadding(new Insets(10.0));
        progressInfoContainer.setPrefHeight(20.0);

        FileTransferTask transferTask = new FileTransferTask(p);
        bar.progressProperty().bind(transferTask.progressProperty());

        transferList.getChildren().add(progressInfoContainer);
        Thread t = new Thread(transferTask);
        t.start();
    }

    @FXML
    protected void onConnectionClose() {
        if (client != null) {
            client.closeConnection();
        }

        status = null;
        user = null;

        displayTable.setRoot(null);
        displayTable.refresh();

        connectButton.setDisable(false);
        disconnectButton.setDisable(true);
        uploadButton.setDisable(true);
        downloadButton.setDisable(true);

        hostnameField.setDisable(false);
        portField.setDisable(false);
        usernameField.setDisable(false);
        passwordField.setDisable(false);

        client = null;
    }

    @FXML
    protected void onApplicationQuit() {
        System.out.println("INFO: Quitting application...");
        onConnectionClose();
        Platform.exit();
    }

    private AuthorizationData user;
    private AuthorizationResponse status;

    private TreeItem<FileTableEntry> rootFile;
    private FileClient client;

    private final static Path rootDir = Path.of(System.getProperty("user.dir"));
    private final static LocalFileSystemAccessor fileAccessor = LocalFileSystemAccessor.getAccessor();
}